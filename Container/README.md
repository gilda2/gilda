to run gilda run the all in one pod 
make sure to adjust the .env and enviroment.ts files 
and run the build and node to start gilda server

in this git there are 3 examples 
kubernetes:
  all in one - all of the gilda components in one pod 
  seperete - each component in a pod 
docker:
  run the system in docker stand alone 

make sure to update the config files and set the correct url of gitlab in the deployment of gitlab and in the config files 

1) start the gitlab server 
2) create admin access token
3) create oauth access for http://gildaserver:3000/oauth/redirect
4) in order to allow access from gilda to git the users must be domain user 
   to allow domain user in gitlab edit the gitlab.rb

       gitlab_rails['omniauth_enabled'] = false
       gitlab_rails['prevent_ldap_sign_in'] = false
       gitlab_rails['ldap_enabled'] = true
       gitlab_rails['ldap_servers'] = {
         'main' => {
         'label' => 'GitLab AD',
         'host' =>  'mydomain.com',
         'port' => 389,
         'uid' => 'sAMAccountName',
         'encryption' => 'plain',
         'verify_certificates' => true,
         'bind_dn' => 'CN=gituser,OU=useraccounts,DC=mydomain,DC=com',
         'password' => 'password',
         'active_directory' => true,
         'base' => 'DC=mydomain,DC=com',
         'group_base' => 'DC=mydomain,DC=com',
         'admin_group' => 'gitGroup'
         }
       }
5) build the gilda image from node (use the Dockerfile - docker build -t node:base .) 
