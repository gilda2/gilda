<!--
PLEASE HELP US PROCESS ISSUES FASTER BY PROVIDING THE FOLLOWING INFORMATION.

ISSUES MISSING IMPORTANT INFORMATION MAY BE CLOSED WITHOUT INVESTIGATION.
-->

## Feature request
<!-- Describe the feature. -->

## What is the motivation / use case?
<!-- Describe the motivation or the concrete use case. -->

## Environment
<!-- Is this feature related to a specific environment (Browser, Platform, etc.)? -->

## Other
<!-- Anything else relevant? -->

