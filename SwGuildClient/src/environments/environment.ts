// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,  
  apiUrl : 'http://gilda:3000',
  clientID: '75f2f84ca648ab593be10eaef6189182d7f6661242bd6c70fd9076c52575154a',
  oAuthProvider:  'http://gitlab',
 
};
