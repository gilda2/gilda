import { Component, ViewEncapsulation } from '@angular/core';
import { MatIconRegistry, MatDialog } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { MatGridListModule } from '@angular/material/grid-list';
import 'rxjs/add/operator/filter';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class AppComponent {

  selected: string = ''

  navItems = [
    { name: 'SCORE BOARD', route: '/scoreboard' },
    { name: 'DEVELOP YOURSELF', route: '/develop' },
    { name: 'ABOUT GILDA', route: '/about' }
  ]








  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private dialog: MatDialog) {
    // To avoid XSS attacks, the URL needs to be trusted from inside of your application.
    const avatarsSafeUrl = sanitizer.bypassSecurityTrustResourceUrl('./assets/avatars.svg');

    iconRegistry.addSvgIconSetInNamespace('avatars', avatarsSafeUrl);
    this.select(this.navItems[0].name)
    //console.log(this.selected)
  }

  select(selected: string) {
    this.selected = selected
  }

  isSelected(nav: string) {
    return nav == this.selected
  }
  openAdminDialog() {


  }

}
