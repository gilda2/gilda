FROM node

RUN apt update && apt install -y vim 
RUN echo alias ll=\'ls -lah\' >> /root/.bashrc

CMD mkdir -p /gilda/gilda
CMD mkdir -p /gilda/gilda/SoftwareGuildServer
CMD mkdir -p /gilda/gilda/SwGuildClient
COPY SoftwareGuildServer /gilda/gilda/SoftwareGuildServer
COPY SwGuildClient /gilda/gilda/SwGuildClient
COPY Container/usefullFiles/client-package-lock.json /gilda/gilda/SwGuildClient/package-lock.json
COPY Container/usefullFiles/server-package-lock.json /gilda/gilda/SoftwareGuildServer/package-lock.json
COPY Container/docker/node-client-config.txt /gilda/gilda/SwGuildClient/src/environments/environment.ts
COPY Container/docker/node-server-config.txt /gilda/gilda/SoftwareGuildServer/.env

#work on client
WORKDIR /gilda/gilda/SwGuildClient/
RUN npm install 
RUN npm install rxjs@6.0.0
RUN npm install rxjs-compat@6.0.0
RUN yes N | npm install -g @angular/cli
RUN ng build 
RUN cp -r dist ../SoftwareGuildServer/src/app/

#work on server
WORKDIR /gilda/gilda/SoftwareGuildServer/
RUN npm install 
RUN npm install -g typescript
RUN npm install -g pm2 

COPY Container/usefullFiles/runGilda.sh /gilda/gilda/runGilda.sh
RUN chmod a+x /gilda/gilda/runGilda.sh

ENTRYPOINT /gilda/gilda/runGilda.sh





